# The Fatpocalypse

[*Return to mods index*](./swappermods.md)

*In an alternate reality, the Hu and Y twins have collaborated and distributed free food to the hardcore needy. However, the rations were laced with an addictive mind control drug. As a consequence, obesity is on the rise, and they have taken over the streets with their army of overweight individuals.*

*The police have taken precautions by arming their officers with better equipment and training. However, they still dare not oppose the grip of the syndicate.*

*Thus, it once again falls to the hands of the city's vigalantes to take to the streets and make them free once again. Have they got what it takes to survive the Fatpocalypse?*

Good luck model citizen! 👻

![The Fatpocalypse](../assets/images/Mods_Fatpocalypse.png)

## Available versions

| Versions | Description |  
| --- | --- |
| [**Fatpocalypse+ v2.2**](../assets/modfiles/FuzzYDs_Fatpocalypse+_v2.2.swap) | Balanced version for Mania+ |
| [**Fatpocalypse v2.2**](../assets/modfiles/FuzzYDs_Fatpocalypse_v2.2.swap) | Balanced version for Mania (requires special loading steps) |
| [**Fatpocalypse+ v1.1**](../assets/modfiles/FuzzYDs_Fatpocalypse_v1.1.swap) | Designed as a joke, wasn't intended to be beatable |

## Features

* Swaps that are intended to try and keep the fights as mixed as possible
* Re-scaled consumables (overall, more chickens/stars) and other resources (stronger weapons) to increase the chances of surviving the Fatpocalypse.  
See [here](../assets/images/item_breakdown.png) for a detailed breakdown of how consumables were adjusted.
* The player gets 125% defense, 75% special usage and extra lives to start. The score required for extra lives in stage and arcade has also been reduced to 7.5k and 20k for a more balanced experience
* Enemies are "shader coded" to give the player an idea of how tough they are.  
e.g. Normal <= 50 hp, Motion blur <= 100, and so on
* A couple of surprises and easter eggs have been included here and there

## Notes

* This mod was originally designed as a meme/survival experience, but has been reworked (v2.0+) with the intent to make it more accessible.
* I strongly recommend to run (v2.2), as it was made with the latest version of the swapper, where I was able to adjust the health and speed of all Bens to match the original character they were swapped with. It should also be stable (without soft locks)
* Some minor adjustments were made after the recording of the video, to make some of the fights a bit more interesting.
* Give boss rush a try too! I was testing it and it turned out to be quite a fresh experience

## Known issues

* When loading the Mania variant, the Swapper (as at 4.2.4), does not load the correct "based on" difficulty. It was intended for **Mania** to be the base difficulty, and **NOT Mania+**. It is recommended to manually correct this (including other customized numbers), if you wish to run the mod on Mania difficulty.

## Special thanks

* Honganqi for blessing us with the swapper tool and making this possible
* Anthopants and Samson6000 for their feedbacks about the difficulty of the mod
* Bellum (BEANS -) for motivating me to work on the balance update to this mod
* Pato for reporting the soft lock in v2.0
* 'I Am Me' for giving me some additional ideas for character names  
(Sorry dude ended up going with the name Hamburglar :x)

## Completed runs

A table of players that have survived the Fatpocalypse!

| Player(s) | Version completed | Character |
| --- | --- | --- |
| Pato | v2.0 | [Axel](https://www.youtube.com/watch?v=cdl1uiEmGFw) |
| Samson6000 | v1.1 (with minor adjustments) | [Axel SOR1](https://youtu.be/Ee1exX1GjxY) |
| BEANS - | v1.1 | [Max](https://www.youtube.com/watch?v=T7cp9ot29rA) |
| FuzzYD | v2.0 (beta - sample run) | [Blaze](https://www.youtube.com/watch?v=GjEevbJSvUs) |

If your run is missing from this list, please do let me know (I may have forgotten to update it)

## Changelog

v2.2:

* Stage 10: Adjusted a character to become a mini boss
* Stage 11: Fixed a bug with one of the mini-bosses not activating
* Customized more character names to be within the theme of the mod

v2.1:

* Fixed a bug with the final boss fight that could cause a soft lock
* Re-worked the Stage 8 boss fight, as it was a bit too easy given the consumables provided for the fight
* Gave a small HP buff to one of the end-game bosses

v2.0:

* Re-balanced the swap for the general audience
* This version is intentionally not made available as it can soft-lock at the end-game

v1.0:

* Initial release, troll version