# Stage 09: Y Tower

[Return to index](./tipsandtricks.md)

[![Blaze Stage 9](./../assets/images/Splash_S09_Score.png)](https://www.youtube.com/watch?v=aYo0Ras6BRY)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

The elevator fight is fun, but the sauna fight is probably my least favorite room in the game. I've learned a lot since completing this, so this guide is written to include theoretical improvements I would have made to the strategy at the time of it's completion. These are marked with the tag [TI]

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Average)](#1-start)
2. [Sauna (Bad)](#2-sauna)
3. [Locker room (Good)](#3-locker-room)
4. [Hallway (Good)](#4-hallway)
5. [Elevator (Average)](#5-elevator)
6. [Boss (Good)](#6-boss)

## 1. Start

### 1.1 Opening

* N/A

### 1.2 Strategy

* Kill **Altet** with 3-hit, charge, forward special
* Wait for **Donavans** to approach, jab them into sync, the blitz to kill
* Jump forward kick to get first strike on the entering **Raven**
* When the rest start coming, forward special, then neutral special for safety
* [TI] Air special to open on kickboxers may be more stable
* Rest of this section requires some freestyle

### 1.3 Freestyle

* Focus down the kickboxers first, using grab combos, throws, specials
* Grab and release enemies to hold the combo if **Kevin** refuses to close in on you
* Watch out for headbutts and stray punches

## 2. Sauna

### 2.1 Opening

* Kill the **Dylan** hiding behind the pillar as you like

### 2.2.1 Strategy - Kevin & Francis

* It's very hard to play this part consistently. I'm only able to provide a general idea of what to do
* Be careful with jumps in this section, as the enemies like to anti-air you
* Try to kill 1-2 **Kevins**, then go for the apple
* Try to kill the **Kevin** entering at the bottom. If enemies approach too fast, grab and throw him
* Quite a bit of freestyle is needed from here
* When there are few (1-3) left (ideally a **Francis**), use jump forward kicks to advance forward. This is to get a better positioning to kill the kickboxers on entry
* Best case is that **Francis** dies before the kickboxers enter
* [Optional] Take the chicken if you can, I couldn't find a good way as it would ruin my positioning

### 2.2.2 Strategy - Kickboxers, Kevin & Francis

* Wall combo (FC) the entering **Ravens**
* Turn around, jump kick and forward special the incoming **Raven**
* Try to Wall combo (FC), jump kick to kill the entering **Condors**
* Try to kill more incoming enemies with freestyle. Use a star as necessary
* Leave one enemy alive, and use them to position to extend to the next section

### 2.3 Freestyle

General:

* When grabbed from front, most enemies here can be killed with forward combo, charge, neutral special/forward special. This depends on the situation
* Try to use grab/release to hold the combo if enemies aren't close enough
* [TI] Air special may be a good counter to many enemies here. It's also better for positioning

**Kevin**:

* Will try to avoid you if you try to close in
* Will rush up behind you fast if your back is facing him, be careful of this

**Francis**:

* May back off really far, causing the combo to drop
* Can be used to advance right, cannot anti air you
* Counter his kick with forward special

**Kickboxers**:

* If they back off slowly, they want to jump you
* If they approach fast, they want to kick you
* You can whiff a jab when they're close to trigger their block
* You can use air cancel to cancel their block on their wake
* Blocks have a timer, so it's safe to move forward and grab them immediately after they block
* [TI] Use air specials to stay safe from them
* [TI] Use jab and neutral special, or jump forward kick/jump down attacks into air special (only when safe) to pressure them
* [TI] Jab and forward special could be another option, but would be situational as it can leave you vulnerable on recovery

## 3. Locker room

### 3.1 Opening

* Assuming you have a grabbed enemy and are positioned correctly at the right wall
* Blitz into jump kick (this breaks the obstacle), then air ride your way to the apple and take it

### 3.2 Strategy

* Kill **Dokuja** entering from the right with 3-hit flurry twice, charge, forward special
* Position upward a bit, 3-hit flurry the incoming **Dokuja** twice
* If the other Dokuja opened with a block, you should have enough time to charge and forward special (better scenario)
* Otherwise, you need to forward special, jump forward, and forward special to kill the second (worse scenario) **Dokuja**
* Position to hit the next two entering **Dokujas** at the same time
* Try to 3-hit flurry them twice, charge into neutral special, and they should die
* Kill the entering **Ravens** with forward special, jump kick, air special
* Kill the remaining **Condor** with freestyle
* Position to attack both entering **Goros** as the same time
* 3-hit flurry twice, blitz, jump kick and double air special
* Time a neutral special on their wake to kill them
* For Pheasants, it's freestyle again. You may need to spam specials a bit to deal with them
* The last Pheasant can be used to position closer to the right wall to extend onwards

### 3.3 Freestyle

**Condor/Pheasant**:

* Please refer to 2.3 for ideas on how to deal with them

## 4. Hallway

### 4.1 Opening

* Break the first vase, take the item, then air dash and forward special
* Take the money, then jump towards the star
* The next part is quite trivial

### 4.2 Strategy

* Try to kill the **Irons** while kicking **Silver** down to keep him alive
* Kill the **Irons** with grab combo into charge attack to avoid too much "splash" damage
* Kick Silver left and right to get the items at the bottom of the screen (i)

Notes:

(i) Silver died too fast, so I couldn't take all the items in the video

### 4.3 Freestyle

**Bodyguards**:

* Will often try to circle behind you to shoot, try not to let them

## 5. Elevator

### 5.1 Opening

* This section is all about watching for visual cues and timing. This is very easy to screw up
* The first objective to break all the glass in the first bodyguards wave
* Begin by moving left and down diagonally, then immediately left and up to the wet floor sign
* This should bait out the attacks from the bodyguards to your left and right

### 5.2.1 Strategy - Bodyguards

* Break the sign to allow **Iron** to come for you
* Sometimes he might not, so you need to jab the one closer to you
* The good thing about this position is that they will take awhile longer to path to you, making it easier to break the glass
* Jump kick Iron, then just auto-combo him against the wall
* Watch out for bodyguards sneaking up from behind, and dodge them with defensive special
* Turn around and jab to hit-stun the sneaky ones, grab them and throw them out
* [TI] Jump down attack could be useful for avoiding damage at this part
* Try to keep a **Silver** alive, and kick him to the right wall, then break it with jump kick, auto-combo like before

### 5.2.2 Strategy - Remaining waves

**Donavan/Signal**:

* If you're in a good position, just jump kick the **Signals**, and forward special **Donavans**. Otherwise, you may need to use grabs and throw them out

**Goros**:

* Time a forward special right the moment you see their shadows appear
* Walk a bit left, and do the same for the next Goro
* Take the apple to hold the combo

**Jumping girls/Kickboxers**:

* Position at the "10-o-clock" of the Y tower symbol on the floor
* Forward special left, then immediately right
* Inch right a bit, then forward special left
* Jump forward kick to the right to kill the entering **Ruby**
* Grab **Condor** and throw him backwards into the **Garnets**
* Jump forward kick to finish them
* [TI] Alternatively, use air dash/forward special
* Take the turkey

**Bens**:

* Position at the"2-o-clock" of the Y tower symbol on the floor
* Time a forward special right when you see shadows appear
* Walk/hop left, forward special again
* Walk/hop right, forward special one last time
* Take a suitcase to hold the combo, and a pipe if you can

### 5.3 Freestyle

* If the timing got messed up, it can be quite hard to recover as you may get knocked out a lot
* Depending on which wave the timing was lost, different enemies will be present. You'll need to deal with them accordingly
* [Donavans/Signals] May be possible to recover with forward jump kicks
* [Goros] Grab and immediately forward special or neutral special them out
* [Jumping girls/Kickboxers] Might be possible to save with air specials, but I can't say for sure
* [Bens] They seem to get stuck while rolling away, which makes it a bit easier to knock them out

## 6. Boss

### 6.1 Opening

* Throw the pipe at Max, then break the cans

### 6.2 Strategy

* This is a freestyle fight where you need to counter-play what Max does

### 6.3 Freestyle

General:

* Max seems to do his attacks with telegraph if you're too close to him on wake, or if his move is cancelled. I don't see this happen much with this strategy, but it comes at the cost of a slower fight

First phase:

* [Flashing red] Punch a few times, jump kick, air cycle
* [Shoulder tackle] Punch a few times, jump kick, air special in the direction of the tackle
* [Spinning attack (while cornered)] Jump kick, air special to safety
* [Spinning attack (not cornered)] Out of range him with back attack

Second phase:

* [Flashing red] Punch a few times, jump kick, air special twice to safety
* [Shoulder tackle] Punch a few times, jump kick, air special away from the tackle, forward special him, then neutral special
* [Spinning attack (while cornered)] Jump kick, air special to safety [TI]
* [Spinning attack (not cornered)] Out of range him timed forward specials, while inching away
* If you land next to him after he starts a telegraph, you probably have to star and wall combo to avoid damage
* Take the food as needed

## Theoretical maximum score

Estimated: **128k+**

Lost points:

* Pickups: 5.9k
* Combo bonus: N/A - Francis skip trigger doesn't have as much damage
* Health: 100
* Stars: 1k
* Time: Variable
