# Stage 03: Cargo Ship

[Return to index](./tipsandtricks.md)

[![Blaze Stage 3](./../assets/images/Splash_S03_Score.png)](https://www.youtube.com/watch?v=Z7Jg_wOPSRg)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

Probably my favorite stage in the game because it's short and sweet, and it didn't feel too hard or easy. This was completed starless as I was curious to know if it there was a feasible way to do so (it is somewhat I guess?).

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Good)](#1-start)
2. [Container area (Average)](#2-container-area)
3. [Engine room (Good)](#3-engine-room)
4. [Corridor (Good)](#4-corridor)
5. [Nora (Average)](#5-boss)

## 1. Start

### 1.1 Opening

* The easiest/most consistent way to do this part isn't in the video, but a speed-running strategy

### 1.2 Strategy

* Jump forward once, then jump forward and kick left. Hold charge attack
* Release the charge on **Dylan** when he gets close, then jump forwards and kick (to try ensure BT is near the can when it blows)
* Move up a bit and grab Kevin, then jump twice to vault and release, into a kick to the left
* I find it takes a bit of luck, but the above can sometimes instantly wipe all the mobs
* Otherwise, deal with remaining enemies coming towards you as convenient

### 1.3 Freestyle

* Try not to position at the center, as it's easy to get swarmed there
* To extend the combo you need to ensure you're close to the right before the last enemy dies

## 2. Container area

### 2.1 Opening

* Similar to the previous section, it's easier to use the speed-run opening for this part as it has a safer setup
* Jump forward and kick **Francis**, then air special into a charge attack to hit the fire can. Walk away in cool fashion

### 2.2 Strategy

* The next part is still a bit unstable for me, but I think a forward special, then neutral special should be the safest way to clear out the **BTs** and **Dylans** 
* You may need to neutral special more than once to ensure safety from all the **BTs**
* Position near the top where the second Francis drops, and forward special him into the barrel. Moving forward a bit can result in Galsia also dying
* Grab the goodies from the container, and forward special the * The next part is still a bit unstable for me, but I think a forward special, then neutral special should be the safest way to clear it. You may need to neutral special more than once to ensure safety from all the **BTs**. away as necessary
* Re-position to the right of the screen and kill the remaining **BTs** there

### 2.3 Freestyle

* Don't fight in the container, you can't jump there and sometimes **Galsia** can his you from inside
* **BT** is probably the most dangerous enemy here because of his quick jabs. Try to kill him as fast
* Also be careful not to jump near **Dylan**, or you might get anti-aired

## 3. Engine room

### 3.1 Opening

* There is a "sweet spot" for the next part. If you're standing there, you can punch **Galsia**, and neutral special when the two **Francis** enter to get them near the barrel
* Then, just forward special the barrel and it should kill everyone near it
* Kill the remaining **Kevin** and **Galsia** and enter the engine room

### 3.2 Strategy

* Move forward and forward special **Ben** twice, then hold charge
* Wait for the barrel to explode, then release the charge and immediately forward special **Ben** on his way down
* Position at the center of the screen and time wall combo (SC) to kill Ben and the others
* Advance forward and kill **Galsia** and **BT** as convenient
* Use a wall combo (SC) to kill two of the **Kevins** as they enter
* Move towards **Donavan** and use a 4-hit flurry into defensive to hook him to the left
* Do another wall combo (SC) to wipe all the entering mobs. Sometimes there may be survivors here
* Kill **Francis** however convenient, then take the pipe (and loot) and proceed

### 3.3 Freestyle

* For the fight with Francis, the strategy description has been enhanced from the time of play. It's safer to use the auto-combo hook on **Donavan** compared to throwing him backwards
* To deal with survivors, just improvise with forward and neutral specials. The section is pretty trivial

## 4. Corridor

### 4.1 Opening

* Advance forwards and hit the **Kevins** with the pipe, throwing it as necessary

### 4.2 Strategy

* I prefer to start by positioning at the first set of oxygen tanks, then just let them come
* There will be a few pairs of enemies approaching, just punch them
* Eventually, a **Donovan** will comes with a hammer. Neutral special and charge attack him
* After that, a **Galsia** and **Kevins** should start coming, but the **Kevins** will back off, so throw the pipe at them to hold the combo
* Advance until **Donovan** aggros you, then jab him until the **Kevins** and a **Dylan** comes from the right. Kill the the **Kevins** but try to leave **Dylan** alive (use a low damage attack on him) for extending to **Francis**
* Be careful not to cross the second set of oxygen tanks, or Francis will start attacking
* Two more pairs will come from the left. Kill them all, then try to get hold of the last Dylan to advance right (with a blitz, jump forward kick)
* To deal with **Francis**, just forward jump kick them both, or use a double air special. Back off a bit, then jump them a second time into a blitz and kill

### 4.3 Freestyle

* Sometimes you need to just jab enemies to buy time while waiting for **Kevins** and **Dylans** to commit to attacking. Once you see them squat, they're committed and will come to you
* If you accidentally trigger **Francis**, you'll probably need to use forward specials to stay alive

## 5. Boss

### 5.1 Opening

* There are two openings, starless and with star. I recommend the latter as it's less troublesome
* [**Starred**] Air dash forwards when the fight stars, immediately star, then forward special left a few times (kill a few set of **Galsias**)
* [**Starless**] Bait the super **Galsias** up a bit, then use a neutral special, charge loop (think it takes 4 times) to kill them. If you don't move up, Nora may heal them

### 5.2 Strategy

* This is a freestyle fight. The objective is to get **Nora** pinned near the right wall, while you're facing left, then cheese her with a jab lock there. This is a lot easier said than done
* Only go for the food pickups after all **Galsias** are dead, as it's very easy to get surrounded otherwise
* If you wish to min-max your score, you need to make sure nothing hits the spiked wall, as you don't get points for it. You also need to ensure no Galsias get whipped, as they don't give you points after that

### 5.3 Freestyle

**Nora**:

* One way to get Nora to your left while jab locking her, then use a jump down attack to switch sides. The timing is quite tight though, and Nora will sometimes start moving before you can
* Be careful when standing next to Nora when she's waking, as she will sometimes immediately whip you twice. A jump kick can sometimes allow you to avoid this, but not always
* Try to ensure there are no **Galsias** between her and you at any point in time. Forward special if there are any to prevent them from getting whipped
* If you're hitting Nora and **Galsia** sneaks up behind, neutral special and charge attack Galsia to stay safe
* Use a star if any **Galsia** gets whipped, or he's going to keep interrupting you

## Theoretical maximum score

Estimated: **88k+**

Lost points:

* Combo bonus: Variable (a bit hard to manipulate this)
* Time: Variable
