# Stage 05: Underground

[Return to index](./tipsandtricks.md)

[![Blaze Stage 5](./../assets/images/Splash_S05_Score.png)](https://www.youtube.com/watch?v=Re5FbqBIbJU)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

A rather tricky level, with satisfactory solutions to most of the rooms. I had intended to enter the retro stage in the video, but broke the machine by accident (it's fine though as its a risky extension). A section is included below to describe what would have been done had I entered.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Good)](#1-start)
2. [Floor traps (Good)](#2-floor-traps)
3. [Wall traps (Average)](#3-wall-traps)
4. [Bridge crossing (Average)](#4-bridge-crossing)
5. [Koobo and Bens (Average)](#5-koobo-and-bens)
6. [Bar fight I (Good)](#6-bar-fight-i)
7. [Bar fight II (Average)](#7-bar-fight-ii)
8. [Retro stage (Average)](#8-retro-stage)
9. [The Red Demons (Average)](#9-the-red-demons)

## 1. Start

### 1.1 Opening

* The idea is to try position yourself between the poison pools as fast as possible, for safety
* Advance right and kill the **Galsias** with a 3-hit flurry then blitz

### 1.2 Strategy

* Nothing fancy required for this part, just use the best kill combos you know to kill enemies
* If there are too many of them, use forward or defensive special to stay safe

### 1.3 Freestyle

* Watch out for **BTs** as they have really quick jabs. Don't let them crowd around you, it's a higher risk of getting hit
* **BT** loves to try and circle around you as well, so keep an eye on him trying to sneak up
* Watch out for **Donovan**/**Altet** coming at you with a pipe. You can either jump kick of forward special to knock them away if they do

## 2. Floor traps

### 2.1 Opening

* [Optional] Collect any pickups you don't already have
* Approach from the top, and start punching the incoming **Donavans**, and kill them however it's convenient
* If they're out of sync, forward special them away as it's a high risk of getting hit. Pick them off with forward specials if they're out of range
* Try not to advance forward too much, as it makes it harder to wall bounce against the left wall, and will also trigger the jumping girls

### 2.2 Strategy

* Wall combo (SC) the two incoming **Signals** to kill them
* Move down a bit, and wall combo (SC) the bottom **Kevin** to kill him
* Do the same for the top **Kevin**. The reason to not kill them together is that the **BT** coming from the right is stupid and usually gets delayed by the floor traps
* Kill the **BT** that should have reached you by now
* Two **Altets** will approach one at a time. Ideally you want to kill them at the same time to avoid having to deal with **Rubies** and an **Altet**
* Do two 3-hit flurries on the first **Altet**, then a jab to knock him down
* Do one 3-hit flurry on the second **Altet**, then forward special, and they should die together
* Position yourself, then wall combo (SC) the Rubies, and neutral kick them to kill
* Walk right, slightly past the turkey, then air dash them twice. This usually gets all jumping girls.
* Follow up with a blitz, double jump down attack twice, then and a charge attack to finish

### 2.3 Freestyle

**Jumping girls**:

* They will sometimes break pattern. Safest way to deal with them is to counter their jumps with air specials, then punish them when they wake
* If you started a blitz but missed one behind, you can cancel into an air cycle to stay safe

## 3. Wall traps

### 3.1 Opening

* [Optional] Grab the turkey, then immediately take the knife and throw it right to extend
* Jump over to the wall trap section

### 3.2 Strategy

* This is a tricky freestyling section, because there are a lot of variations to how the **Dylans** will approach

### 3.3 Freestyle

General:

* Try not to move too far forward, to avoid triggering all Francis coming out of sync
* Try to avoid using neutral special, as it moves you backward and could get you hit by the wall trap
* When there's only 1-2 enemies left, you'll want to try and air dash to the right wall to prepare to advance to the next section

**Dylans**:

* There are 3 rows that they can approach from, I prefer to start from the middle one
* Any **Dylans** approaching from the bottom usually get hit by the wall trap
* If Dylan is quite close, air dash him and finish him off
* If he's a little farther, you'll need to forward special as the air dash can miss

**Kevins**:

* Will usually try to sneak up behind, you could back attack him and quickly continue hitting whatever is in front of you. Defensive special also works, but bears the risk of getting hit by a wall trap

**Francis**:

* If he tries to jump you, forward special to push him back

## 4. Bridge crossing

### 4.1 Opening

* Grab the money, then move diagonally up and left, then jump forward and kick backwards to make the Altets fall into the pit
* Note: I played this section a little differently in the video as I wanted to leech my life back (but failed)

### 4.2 Strategy

* Advance forward, and punch Donavan a few times until you can forward special all incoming enemies at once
* Jump kick forward and hold charge attack, then release it when the enemies are close enough. Try not to advance too far to avoid triggering **Francis**
* Take a knife, then forward jump kick **Francis** to delay his approach
* Move down to the barrel, back attack it, and immediately throw the knife towards the second barrel. If you're lucky, this will hit **Signal**
* Get out of the poison pool, jump kick **Francis** one more time, then kill him (and hopefully **Signal** too) on his wake
* **BTs** and **Galsias** should be coming now. This is your chance to take all the loot. Use forward jump kicks to keep the combo alive while doing so. I usually take the star, apple, then suitcase. This puts you in a perfect position to extend to the next part
* After taking all the loot, BT should be close to you while taking the suitcase. Jab him once, then grab him, and time a throw and jump to cross the hazard wall
* Wait a short while to take the turkey if you wish to extend
* The next part with jumping girls needs a little freestyle

### 4.3 Freestyle

**Jumping girls**:

* Start at the bottom of the screen, defend against them with forward and defensive specials until 1-3 remain
* Reposition to the right wall when they're close to death, and try to kill them there (to extend to Koobo)
* If you're lucky, you'll manage to grab one after side-stepping to dodge her jump. This is the best case, as it makes it easier to extend
* Otherwise, they'll likely all die to the hazard wall, so you need to be hugging the right wall to make it to Koobo

## 5. Koobo and Bens

### 5.1 Opening

* Jump forward twice, then Jump down attack to open on **Koobo**

### 5.2 Strategy

* Do a 2-hit, followed by 3-hit flurry, then charge attack
* Break the apple tin, wait for the Bens to charge, then pick it up and move upwards to dodge the Bens
* Step out of the corner, punish the Bens with whatever means possible. This part requires freestyle, and usually a star too. It takes quite a bit of practice to consistently get it right, as it depends on the situtation

### 5.3 Freestyle

* It's safe to flurry the Bens as long as Koobo hasn't gotten into a position to attack you.
* If Koobo comes from the front, forward special, blitz, double down attack, then air special
* If he is coming from behind, you need to 4-hit flurry ben into defensive to hook them backwards
* For both the previous points, you will likely need to use a star, and follow up with a charge, forward special to kill them all

## 6. Bar fight I

### 6.1 Opening

* Approach and grab **Kevin**. Kill him with 3-hit grab combo into charge, forward special, jump kick
* Ideally, all **BTs** will be in front of you now. Try to deal the finishing blow on them while you're in range of Donavan to extend

### 6.2 Strategy

* Forward kick **Donavan** into the table, then do it once more to get him closer to the wall
* Break the second table for it's loot, then forward jump kick through the chair to knock Donavan down again
* Grab the apple, and money, then grab Donavan and release him until you're aligned with the pillar
* Grab forward attack him 1-2 times until he's 1-2 hits to death while doing the previous step
* Blitz into double air special to position right in front of **Honey**
* Immediately forward special to counter her shin kick after the cutscene, then air dash twice (with a slight delay on the second dash) to position near the right wall. Use a star to kill all the Sugars
* More will enter, immediately defensive special, then move left a bit, jump left and kick right. This will usually get them all against the wall
* Forward special, then blitz, and jump kick to thin the herd
* Jump kick 1-2 more times until only 1 Honey is left
* Grab the last Honey, hit her until she will die to a throw, then throw her. Avoid killing her against the wall, as this will make the **Donovans** immediately come for you

### 6.3 Freestyle

**Biker girls**:

* If the biker girls spread out too much, then the only other way to deal with them is to let them come one at a time, and punish them after their headbutt
* Watch out for the hitbox, it lingers awhile after their charge animation ends
* Don't walk too close to them as they can suddenly kick you
* They are most vulnerable if you are able to get them against the wall, and in a group

## 7. Bar fight II

### 7.1 Opening

* For this part, it's a lot easier if you don't break the pool table
* I screwed up in the video and broke the arcade machine, not recommended to do this
* Grab the loot in the chairs, then bait **Altet** and forward jump kick him down

### 7.2 Strategy

* While **Altet** is knocked down, take his pool cue and throw it at **Galsia** at the right
* Kill **Altet** with 3-hit, blitz, charge attack, then cross chair to trigger the **Bikers** entering
* Punch the incoming Bikers, and forward special as necessary to stay safe. They're in a confined space so it should be quite easy to kill them
* Kill any **Bikers** that manage to path out of the pool table however convenient
* When no **Bikers** are able to come, move down a bit and forward special the table
* Kill the remaining **Bikers** and **Donavan** using the freestyle tactics in 6.3. Throwing pool cues at them is also a good way to do this
* Save the 8-ball for **Kevin**, as long as you position yourself right, it should kill him instantly
* Take a pool cue and break the table with the star, drop the cue and pick up the star while facing backwards. This should bait **Signal** to come for you
* When Signal gets close enough, hit him a few times, then grab, vault, and blitz. This should kill him, and if you did it right, also trigger the knife **Galsias** to come in. Otherwise, walking up to the chair and punching it (if it didn't already break), should do the trick
* For the next part I usually take the bottle, and whatever else I can kill to throw the Galsias
* [Optional] Use projectiles to break the obstacles near the pillar at the right. It can take 3-4, and you can sometimes kill the **Galsia** standing near the taser before **Caramel** spawns
* Jump forward to trigger **Caramel'** entry. If you're lucky, all **Galsias** should be dead by now
* Caramel should be right in front of you, so jab-lock her with 12 hits, then blitz, jump kick and air special twice. Alternatively, you could charge and forward special, but I haven't tried this and am ensure of it's effectiveness
* Dealing with the last 3 Honeys can require a bit of freestyle
* [Optional] To extend into the machine, throw the last Honey backwards, take the taser, and move upward to break the chair. Take the loot in the chair, then taser the machine

### 7.3 Freestyle

**Honeys**:

* Similar to before, you want to try keep them against the wall and just keep jump kicking them to death as they wake
* You can also kill them with blitz, double down attack, and charge, but it depends on whether it's safe to do so
* If any come from behind, jump backwards and kick forwards to get her grouped with the rest
* If you wish to try and extend into the arcade machine, try to keep on **Honey** alive

## 8. Retro stage

### 8.1 Opening

* Move up, then jump down attack the turkey box, but don't pick it up

### 8.2 Strategy

* This strategy isn't so stable yet, a bit of luck is still required depending on how Abadede moves
* After breaking the box, I walk diagonally left and down a bit, and try to catch Abadede with the slam of my defensive special. The slam is followed up by blitz, double down attack, charge attack
* Time a break on the second box to prevent the combo banking, then time a defensive special on Abadede when he approaches (ideally with body slam/charge)
* Assuming he's on the ground, blitz him, then time a star when he's almost awake. If you did it right, you can follow up with a wall combo (SCS), jump kick, air special
* Last of all, immediately blitz, double down attack, and charge attack to finish him
* Time your pickups of the star and turkey to hold the combo
* To extend to the Boss upon exit, take the pickups in this order:  
  Suitcase > Turkey > Apple > Moneybag
* Take any projectile convenient, throw it right and immediately jump to chain to the bar exit area. You may need to throw two projectiles or just do it once and hop over
* Take the foods, loot in bottom right tin, then the top right tin. Make sure to walk backwards a bit to avoid the exit trigger getting stuck on your exit  

### 8.3 Freestyle

* After knocking Abadede down, its possible to vertically offset yourself a bit, then immediately grab and slam him when he tries to come for you. This is a potentially consistent way to deal with him for this stage (but I haven't been able to do it yet)
* Walking slowly towards you - punch and punish him
* Body slam, Charge - side-tep or time a defensive special to knock him down when he loses his i-frames. Blitz and punish

## 9. The Red Demons

### 9.1 Opening

* Move up a bit, then forward special **Barbon** to knock him as far back as possible. Moving up prevents him from colliding with his bike

### 9.2 Strategy

* If you want to take the easy way, just follow the freestyle section on **Barbon**. If you want to farm the Bikers, keep reading this strategy
* Use back attack to break the first bike. Take the loot, then jump down attack the incoming **Sugar**
* Kill her with 3-hit flurry, jab, grab, grab-hit, forward throw
* Repeat the same thing with the second bike and the **Honey** that comes with it
* Break the third bike. This part can depend a bit on luck, as Barbon may begin to aggro
* If Barbon doesn't aggro, jump, then punish the Sugar that charged in
* Otherwise, you will need to use defensive special and forward specials to stay safe while breaking the bikes backwards
* While breaking the last bike at the top, Barbon will usually do his white attack, to which you should defensive special to knock him left
* Wait for him to approach a bit, then take cover behind the bikes. The next part is freestyle

### 9.3 Freestyle

**Barbon**:

* Coming from the front - Approach him diagonally, he will usually whiff his first attack, and you get to punch/punish him when he comes in for the second punch
* Coming from the back - Time a defensive special slam, then follow up with jump forward kick, air special, charge and immediate forward special. Depending on the situation, there are a few ways to follow up on this. For example, into an infinite, or into some flashy combo, depending on what you're able to pull off
* If he's running at you from the front and you don't want to deal with him (for farming bikers), just forward special before he gets too close
* If he has a wall behind him, you could wall combo (SCS) him and follow up with some additional hits
* At the end of the day this fight will be as easy as you're able to hold a combo against Barbon. It's very possible to touch of death him with just one or no stars if you are familiar with the timing of Blaze's infinite

**Motorbike fortress**:

* If you're trying to farm bikers, you need to pay attention to where **Barbon** is trying to go, as he may get stuck pathing either at the top or bottom of his bike. Ideally, you want to make sure you fight in a place that will keep him stuck until all bikers are dead
* Keep the top 1-2 bikes intact, and use back-attack to break all the rest. If you're lucky, all the bikers will come from the bottom, and Barbon is stuck at the top
* Punish bikers coming in after they charge, but try to avoid forward specials, as it can knock them too far away. Defensive special or jump kick may be better options as they don't knock them back too far. Pick up the loot to hold the combo if they take too long to approach
* Once all bikers are dead, move upward and use forward special to push **Barbon** away and break the bikes at the same time
* Wait for the remaining Bikers to approach for their punishment, while pushing Barbon away when they're still alive
* Once all bikers are dead, just use freestyle to finish **Barbon** off. You can jab his bike to keep the combo alive. That's it!

## Theoretical maximum score

Estimated: **149k+**

Lost points:

* Pickups: 1.6k
* Combo bonus: 6k+ (from retro level)
* Health: 740
* Stars: 2.5k
* Time: Variable
