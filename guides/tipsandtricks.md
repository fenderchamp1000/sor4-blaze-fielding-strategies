# Tips & tricks

[*Return* ![back](../assets/icons/icon_home.png)](../README.md)

This topic contains links to guides for every level I've completed full-combos for in to date, in *Mania* difficulty, with the version *Steam, v05-s*. They likely won't work as well for Mania+ or other difficulties, but may be possible to adapt.

As of the latest version *Steam, v07-s* introduced with the DLC, there have been some change in the mechanics of the game. Unfortunately, this has broken the executability of some of the combos I've described in this document. In particular, Blaze's charge into defensive special now has a high risk of "bounce outs", unless the enemy is at a sufficient height in the air.

There are workarounds to the breakage, but styling is not as versatile for Blaze as it was in v05.

## Consistency ratings

*Sections* for each stage will have a consistency rating assigned to them, in brackets.
This is a rough indicator of the success rates I was able to manage with the strategies and freestyle described.

* **Bad**: Less than 30%
* **Average**: Between 30-70%
* **Good**: Better than 70%

For example:

```markdown
Castle (Average)
```

## Structure

Guides for *sections* for each stage are broken down into 3 parts (where applicable):

* **Openings**: Setting up a section before executing the strategy for it
* **Strategy**: Description of the strategy for a section (if a decent one could be found)
* **Freestyle**: For futile attempts to recover from failed executions with counter-play

## Guides

Completed stages:

* [Stage 02: Police precinct](./stage02.md)
* [Stage 03: Cargo ship](./stage03.md)
* [Stage 04: Old Pier](./stage04.md)
* [Stage 05: Underground](./stage05.md)
* [Stage 06: Chinatown](./stage06.md)
* [Stage 07: Skytrain](./stage07.md)
* [Stage 08: Art Gallery](./stage08.md)
* [Stage 09: Y Tower](./stage09.md)
* [Stage 10: To The Concert](./stage10.md)
* [Stage 11: Airplane](./stage11.md)
* [Stage 12: Y Island](./stage12.md)

Other stages:

* Stage 1 is not available yet. I started working on guides from Stage 9, as that is the point at which the balance patch for the game was released around September 2020.
* To date, full-combos have been completed for all stages, but there is still some backlogged content to be written
* The DLC was also recently released, which introduced some changes in the game mechanics. Unfortunately, this means that some of the strategies described here may be broken now :(
