# Stage 12: Y Island

[Return to index](./tipsandtricks.md)

[![Blaze Stage 12](./../assets/images/Splash_S12_Score.png)](https://www.youtube.com/watch?v=I0puGyu6tWU)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

Goes without saying that this is likely the hardest stage in the game. A lot of sections require freestyle, and this guide aims to provide suggestions on how to overcome the trickier ones. The 'Crash-site' and 'Candy' sections aren't covered as they're quite trivial.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Bikers (Average)](#1-bikers) - and Jumping girls (excludes Candy fight)
2. [Bridge (Good)](#2-bridge) - Bronzes and Sparrows
3. [Castle (Average)](#3-castle) - Zs, Jonathans, D. Signals
4. [Baabo (Good)](#4-baabo) - and Zs
5. [Shadows (Average)](#5-shadows)
6. [Bens (Average)](#6-bens)
7. [Miss Y (Average)](#7-miss-y)
8. [Boss (Average)](#8-boss) - After mech appears

## 1. Bikers

### 1.1 Opening

* Grab a knife, approach the crate diagonally
* Forward kick the crate, then forward jump and throw the knife in mid air
* Jump forward, then move diagonally until in line with the bikers

### 1.2 Strategy

* Forward special the **Caramels** away, then inch left a little
* Kill the **Saphyrs** with a wall combo (SC), jump kick, and air special
* Grab the nearest **Garnet**. Make sure to face the wall, vault if needed
* Freestyle is required from this point, depending on which way **Garnet** is facing

### 1.3 Freestyle

First **Garnet**:

* *Facing right*: Kill with grab forward hit (3x), charge, neutral special
* *Facing left*: Suplex, and wait for her wake. Kill with 3-hit flurry, charge, forward special

Second **Garnet**:

* *Jump in*: Counter with neutral special or fast air dash
* If two come at you while out of sync (due to bad RNG), counter the second with forward special.

**Bikers**:

* *Charge* (from front): Counter with timed air cancel, then 3-hit flurry lock to death
* *Charge* (from back, while you're hitting a target near wall): Blitz, jump kick, air cycle to land safely
* Watch out for charges coming from off screen (telegraphed by war cry)
* Don't chase them if they're backing off, they run faster

General:

* Enemies here like to come at you in a line
* Let them come for you, then counter accordingly
* If you manage to knock them all down, forward jump, blitz, jump kick, then air special to close the gap with the next section.
* Finish them off any way you like

## 2. Bridge

### 2.1 Opening

* Inch forward, forward jump, then air dash
* Inch forward again, hit **Bronze** once, then move down

### 2.2 Strategy

* Wait for 3 gunshots, move back up. Kill **Bronzes** with 3-hit flurry twice, charge, forward special.
* Inch upwards to dodge incoming **Sparrow**, 3-hit flurry twice, blitz, jump kick [1], air special
* Wait for both to land, blitz and jump kick to finish
* Second **Sparrow** sometimes decides to block.  
Can recover with immediate air cycle instead of air special after [1], neutral special twice, grab and punish

### 2.3 Freestyle

**Sparrow**:

* *Jump in*: inch up or down to dodge, then punish
* *On wake*: use air cancel to stop super armor flying knee, then punish

**Bronze**:

* Safe to resume attacks on him after sound of third gunshot
* Sometimes breaks formation on open.  
Sadly, I don't have a good way to recover from this (I lucked out in the reference video)

## 3. Castle

### 3.1 Opening

* Break the armor, grab the sword, move forward (critical) and get the money
* Move diagonally down, jump and throw the sword
* Jump forward, then inch forward (critical) and immediately grab and drop the sword 
This baits the Zs to try and pick it up.
* *Critical*: Not moving forward enough seems to cause enemies to break formation

### 3.2 Strategy

* Kill the **Zs** with wall combo (SCS), jump kick, air dash
* Jump right and kick left to get **Jonathans** in the corner
* Inch diagonally up and right, forward special to bounce out the incoming **D. Signals**
* Inch diagonally down and left, do the same for **Jonathans**
* Turn around, 3-hit flurry the waking signals, then 5-hit all of them (knocks them to the right) 
* Jump forward, then inch forward, 3-hit flurry to finish
* Inch forward and tap **Z** to cancel his attack twice, then grab him
* Wait for **Jonathan** to be right behind you, then throw Z backward
* Wait for Z to be 2/3rds awake, then wall combo (SC), jump kick to kill all 5 of them
* Kill the remaining **Zs** while minding not to move too far forward, or **Baabo** will come straight down
* If things didn't go according to plan (quite likely...), you'll need to freestyle.

### 3.3 Freestyle

**D. Signal**:

* Can be killed against wall with 3-hit flurry, charge, neutral special, or variations of it
* Can be baited by facing your back to them
* Watch out for slides, which can be air canceled

**Z**:

* Can be out of ranged and killed with 3-hit flurry twice, blitz, charge
* Don't stand near him on wake, he attacks immediately
* Watch out for flying weapons, side-step, neutral special or grab them

General:

* Forward special if there are too many enemies getting close
* Try to keep enemies to the right, away from weapons
* If there are too many on your left, forward special, neutral special can hook them to the right

## 4. Baabo

### 4.1 Opening

* Use the armor to hold the combo while positioning yourself

### 4.2 Strategy

* Break the armor then inch forward and forward jump kick the second armor
* Inch forward again and time a jump kick on Baabo's landing
* 3-hit flurry twice, suplex, blitz, jump down attack, air special
* Wait for landing, neutral special, forward special right
* If you're lucky they'll all be dead

### 4.3 Freestyle

* Just kill the remaining Zs anyway you like, there isn't a right solution to it

## 5. Shadows

### 5.1 Opening

* Inch forward, throw the mace, then move diagonally up and right
* Air dash over and the first Shadows should spawn
* How they begin to move depends on luck.

### 5.2 Strategy

First wave:

* Forward special, jump kick, then air special left (1 of 7 dead)
* Neutral special, forward special, then hop right once (3 of 7 dead)
* Forward special, jump kick, forward jump kick the other **Shadow** (4 of 7 dead)
* Forward jump left until you kick the next Shadow, then move down and grab the circling one
* Wait until the other almost circles around, grab combo, charge the one you have (5 of 7 dead)
* Forward jump kick the circling one, kill him if its safe (6 of 7 dead)
* If its not safe (his friend circled), kick his friend down first, then grab combo him

Second wave:

* Time a forward special right about 3 seconds after picking up the apple
* Jump kick, air special left, then neutral special
* Finish off the rest with freestyle

### 5.3 Freestyle

**Shadows**:

* Stay safe by doing forward jump kicks left and right
* The timing to dodge their shurikens is when they raise their hand
* Will always try to circle around after knockdown
* If all are circling, try to grab combo or grab throw to thin their numbers

## 6. Bens

### 6.1 Opening

* Fast air dash through the crate
* Approach diagonally until you're in line with the Hearts

### 6.2 Strategy

First wave:

* Forward special to knockdown the **Hearts**
* Jump left, do the same twice to the Bens while moving towards the left
* Blitz, jump down attack twice and air special. Finish **Bens** on wake with jump kick
* Hug the left wall for safety, then freestyle the **Gourmands** and **Hearts**

Fire wave:

* Position slightly below the case, and evade when they run in
* Wait for the flames to extinguish, then punish like in video reference

Jumping wave:

* Position around carpet center, punch and grab the one on the right
* Time a vault to dodge, then blitz them all
* Time an air cancel on their wake, then punish like in video reference

Rolling wave:

* Position at carpet center, open with forward special to hold combo
* 3-hit flurry twice, grab, vault, time a blitz when the others start rolling
* Forward special to kill one, then position to punish the next one incoming
* Squeeze as many hits as you can in, but grab him before his friend starts rolling [1]
* Time a forward slam when his friend is about to hit him [2]
* Friend should end up in nice position for a punish
* Repeat [1-2] as required

### 6.3 Freestyle

**Gourmands**:

* *Belly flop*: Counter by moving up and down, grab and forward slam  
If they're too close or you don't have enough time to move, neutral special
* Can use 4-hit flurry, neutral special to hook them behind you  
This is useful for getting them in the corner for the first wave
* Can safely air cancel them on wake

**Hearts**:

* Try to use forward special to force them away so they don't roll forward and backward  
They're more predictable when only going one way
* Throw stuff at them if they end up too far away, make sure to aim properly
* Best time to punish them is immediately after they finish rolling

## 7. Miss Y

### 7.1 Opening

* Move up diagonally down and right and forward air cancel her  
* If the previous approach fails, move diagonally down and right and try to wall combo (SCS), jump kick, air special, reverse neutral special
* If both the above fail, you'll probably lose the combo and just have to freestyle

### 7.2 Strategy

* If you're able to catch her, execute the longest possible combo string you can on her
* Using the combo string in the video reference twice is enough to beat her

### 7.3 Freestyle

**Miss Y**:

* Can open with wall combo (SCS) if she is close enough to one. She usually won't be able to evade
* *Quick lunge*: Telegraph seems to be pacing in small circle or running towards you  
Counter with forward air cancel when she gets in range, then punish
* *White attack*: Wait for her to get close enough, forward jump and down attack twice, then punish
* *Red attack*: Wait for her to get close enough, air cancel, then punish
* *Zig-zag* (near): Counter with two timed neutral specials while facing her, then punish
* *Zig-zag* (far): Try to get to where she started with fast air dash and a neutral special, then punish
* She is vulnerable for a short time between her zig-zag attacks
* Be careful with stars in this room, they can break armors and make the fight harder
* Hit the guard armors to keep your combo going!

## 8. Boss

### 8.1 Opening

* The combo string in the video example will immediately trigger the mech
* Alternatively, just use Blazes infinite on the twins
* I recommend approaching the fight against the right wall, as its easier to react to some nasty openings from the mech from there

### 8.2 Strategy

* This fight is pretty much all freestyle, and assumes you position your attack from the right center
* The mace is for insurance, and isn't safe to use while Miss Y is still around due to slow recovery frames
* Any stars you still have here are also insurance, against some difficult to counter attacks
* If the mech opens with a swipe left at the bottom of the screen, throw the mace at Miss Y
* If Miss Y is defeated and the mech is still alive, hammer it with the mace

### 8.3 Freestyle

**Mech**:

* *Right-left swipe* (mid screen): Worst attack, as it's hard to react to, and a high risk of a combo break or bank. Hold combo with a star or by moving down to take stuff from boxes
* *Right-left swipe* (bottom screen): Only possible to hold combo if you're able to attack Miss Y. This can be done by throwing the mace or freestyling her if she is approaching
* *Double slam and split swipe*: Also bad, because you either need to go for the bottom right crates or air dash in and use a star to hold the combo.
* *Left-right swipe*: Move right and hit the leg until it's over
* *Up and down slams*: Hug the right wall, and forward special left, then move up a bit and do the same to hold the combo
* *Left and right slams* (Miss Y alive): Time a forward special when the first leg slam comes down. Move right, and forward jump kick into air special. This is a good time to get the star box, and catch Miss Y with freestyle to finish her
* *Left and right slams* (Miss Y dead): Inch diagonally down and left, then slow air reverse to hit the leg in the air. Timed a forward special to hit the right leg coming down. Immediately move up, and time a jump kick when it comes down again. After that, return to the center right position

## Theoretical maximum score

Estimated: **232k+**

Lost points:

* Pickups: 3.3k
* Combo bonus: 8k+ (from lost damage)
* Health: 240
* Stars: 1k
* Time: Variable
